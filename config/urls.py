from django.contrib import admin
from django.urls import path, include
from ads.views.ads import index

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include('users.urls.locations')),
    path('api/v1/', include('users.urls.users')),
    path('api/v1/', include('ads.urls.ads')),
    path('api/v1/', include('ads.urls.categorys')),
    path('api/v1/', include('selection.urls')),
    path('', index),

]
