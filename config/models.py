from django.core.validators import MinLengthValidator
from django.db import models


class BaseModel(models.Model):
    slug = models.CharField(max_length=10, null=True, unique=True, validators=[MinLengthValidator(5)])
    name = models.CharField(max_length=200, null=False, validators=[MinLengthValidator(5)])
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
