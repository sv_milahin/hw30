import pytest

from ads.serializers.ads import AdSerializer
from tests.factories import AdFactory


@pytest.mark.django_db
def test_list_ads(client):
    ads = AdFactory.create_batch(1)

    response = client.get('/api/v1/ad/')

    expected_response = {
        'count': 1,
        'next': None,
        'previous': None,
        'results': AdSerializer(ads, many=True).data
    }

    assert response.status_code == 200
    assert response.data == expected_response
