import csv
import json


def csv_to_json(csv_path, json_path, model):
    with open(csv_path, 'r', encoding='utf-8') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        data = [row for row in csv_reader]

    json_data = []
    for row in data:
        if 'id' in row:
            pk = int(row['id'])
            del row['id']
            if 'is_published' in row:
                row['is_published'] = True if row['is_published'].lower() == 'true' else False
            json_data.append({
                'model': model,
                'pk': pk,
                'fields': row,
            })

    with open(json_path, 'w', encoding='utf-8') as json_file:
        json.dump(json_data, json_file, ensure_ascii=False)


csv_to_json('ads.csv', 'ad.json', 'ads.ad')
csv_to_json('category.csv', 'category.json', 'ad.category')
csv_to_json('location.csv', 'location.json', 'location.location')
csv_to_json('user.csv', 'user.json', 'ad.user')
