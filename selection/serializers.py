from rest_framework import  serializers

from ads.serializers.ads import AdSerializer
from selection.models import Selection


class SelectionListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Selection
        exclude = ['owner', 'items']


class SelectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Selection
        fields = '__all__'

    owner = serializers.IntegerField(source='owner.id')
    items = AdSerializer(many=True)


class SelectionCreateUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Selection
        fields = ['name', 'owner', 'items']