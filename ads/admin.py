from django.contrib import admin


from .models.ads import Ad
from .models.categorys import Category


@admin.register(Ad)
class AdAdmin(admin.ModelAdmin):
    list_display = ['name', 'price', 'is_published']
    list_filter = ['name', 'price', 'is_published']
    search_fields = ['name', 'slug']


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'created', 'updated']
    list_filter = ['name', 'created', 'updated']
    date_hierarchy = 'created'
    search_fields = ['name']
