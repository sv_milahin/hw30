from rest_framework import serializers

from ads.models.categorys import Category


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'