from django.urls import path
from rest_framework_simplejwt import views
from rest_framework.authtoken import views as login_views
from ..views import users


urlpatterns = [
    path('user/', users.UserListView.as_view()),
    path('user/create/', users.UserCreateView.as_view()),
    path('user/<int:pk>/', users.UserDetailView.as_view()),
    path('user/<int:pk>/update/', users.UserUpdateView.as_view()),
    path('user/<int:pk>/delete/', users.UserDeleteView.as_view()),
    path('user/token/', views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('user/token/refresh/', views.TokenRefreshView.as_view(), name='token_refresh'),
    path('api/token/verify/', views.TokenVerifyView.as_view(), name='token_verify'),
    path('user/login/', login_views.obtain_auth_token),

]