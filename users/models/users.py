from django.contrib.auth.models import AbstractUser
from django.db import models
from users.models.locations import Location


class User(AbstractUser):
    class Role(models.TextChoices):
        ADMIN = 'AD', 'Администратор',
        MEMBER = 'MB', 'Пользователь',
        MODERATOR = 'MO', 'Модератор'

    age = models.PositiveIntegerField(blank=True, null=True)
    role = models.CharField(max_length=2,
                            choices=Role.choices,
                            default=Role.MEMBER)
    location = models.ManyToManyField(Location)
    birth_date = models.DateField(null=True)
    email = models.CharField(max_length=100, null=True, unique=True)




    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def __str__(self):
        return self.username
