from django.db import models
from config.models import BaseModel


class Location(BaseModel):
    lat = models.CharField(max_length=10)
    lng = models.CharField(max_length=10)

    class Meta:
        verbose_name = 'Местоположение'
        verbose_name_plural = 'Местоположения'

    def __str__(self):
        return self.name
